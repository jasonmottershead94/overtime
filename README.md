# Overtime
A private group repository for Entertainment Design Group 14 XIVGames - Overtime.

# Tools
-----------------------
* **Unreal Engine 4.9.2**, available [here](https://www.unrealengine.com/download?dismiss=null), make sure that you download Unreal 4.9.2 to make sure that it matches up with the rest of the team and so that it matches the Unreal 4 version in university.
* **SourceTree**, available [here](https://www.sourcetreeapp.com/), make sure to follow the document provided [here](https://drive.google.com/open?id=0Bzmw2R8Or0_Aano4XzdyNTdndnM) to ensure that you have set up GitHub and SourceTree properly. If anyone needs help with using SourceTree, there is a document [here](https://drive.google.com/open?id=0Bzmw2R8Or0_AZ0ZKMXNHZ0UtR0E) to help out with terms and gives small how to guides.

Any questions feel free to email me: 1300455@abertay.ac.uk or contact me on Facebook.
